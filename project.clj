(defproject org.clojars.gdr3941/clj-util "0.1.0-SNAPSHOT"
  :description "My utils for clojure"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.3"]]
  :repl-options {:init-ns clj-util.core})

;; Push to Clojars using
;; lein deploy clojars 
