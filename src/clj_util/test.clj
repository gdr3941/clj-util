(ns clj-util.test)

(defmacro ex
  "Example test. Checks that v = ev.
  Usage: (ex (+ 1 1) 2)"
  [v ev]
  `(clojure.test/is (= ~ev ~v)))

